---
title: "Frequently asked questions"
permalink: /faq/
toc: true
---

Here are some frequently asked questions. If you can't find your answer here,
please check out our [Gitter](https://gitter.im/lely_industries/canopen) room.

## What is Lely CANopen?

A high-quality, feature-rich, open-source implementation of the
[CANopen](https://en.wikipedia.org/wiki/CANopen) protocol, including several
tools to help develop and debug CANopen applications. The project is actively
developed, with over 250 commits in 2019 and over 500 commits in the first three
quarters of 2020.

## Who is using it and why?

As the name implies, Lely CANopen is primarily used by
[Lely](https://www.lely.com/), a large robotics company in the Netherlands. It
is also used by several of its suppliers as well as in multiple projects,
open-source and otherwise, all over the world. Most recently, the European Space
Agency has
[investigated](https://indico.esa.int/event/276/contributions/4539/attachments/3500/4630/W43_-_Karl_-_cis19.pdf)
it for possible use in satellites. We're currently working together with
[N7 Space](http://n7space.com/) to create an [ECSS](https://ecss.nl/)-compliant
version of the stack.

At Lely, we started using it because we were dissatisfied with the commercial
CANopen implementations we were using and thought, perhaps naively, how hard can
it be to write our own? At the time (2015) none of the open-source
implementations met our needs. Since writing a CANopen stack is not our core
business --- although it is mission-critical for our products --- we decided to
make it available to the open-source community. Over time it has grown to
incorporate many features not available in other stacks.

## Which platforms are supported?

Linux is the best supported platform, not least because CAN bus support is
available in the mainline kernel through
[SocketCAN](https://www.kernel.org/doc/Documentation/networking/can.txt).
Windows 7 and later are supported, but only [IXXAT](https://www.ixxat.com/) CAN
adapters have a built-in driver. The stack also supports bare-metal platforms,
using the [Newlib](https://sourceware.org/newlib/) C library, but you have to
write the CAN driver yourself (see
[here](https://gitlab.com/lely_industries/lpc17xx) for an example).

All Lely core libraries are written in pure and compliant
[C99](https://en.wikipedia.org/wiki/C99) and
[C++11](https://en.wikipedia.org/wiki/C%2B%2B11), with a few features from
[POSIX](https://pubs.opengroup.org/onlinepubs/9699919799/). Emulation of those
features is provided for non-POSIX platforms. This means that, in general, it is
quite easy to port the stack to a new platform, as long as a conforming compiler
is available. We have done this recently for the C2000 microcontrollers from
Texas Instruments using their
[code generation tools](http://www.ti.com/tool/C2000-CGT).

## How do I get started?

Follow the [installation]({{ '/docs/installation/' | relative_url }})
instructions, if you haven't already, and dive into the
[command-line tutorial]({{ '/docs/cmd-tutorial/' | relative_url }}) or
[C++ tutorial]({{ '/docs/cpp-tutorial/' | relative_url }}).

## Where is the documentation?

The API is documented with doxygen and can be found
[here](https://lely_industries.gitlab.io/lely-core/doxygen/). On this site you
can find an overview of the [tools]({{ '/docs/can2udp/' | relative_url }}) and
[libraries]({{ '/docs/overview/' | relative_url }}), as well as some information
about the [internals]({{ '/docs/objects/' | relative_url }}) of the CANopen
stack.

## What is the roadmap?

New features can be proposed and tracked in the
[issue list](https://gitlab.com/lely_industries/lely-core/issues). The most
important planned features are:
* Add "logical drivers" for the most used device profiles (CiA 401 and 402 at
  least). Include mock slave implementations of those profiles for testing
  and demonstration purposes.
* Add a tool to scan a CANopen network, configure node-IDs and update the
  configuration and firmware of the slaves
  (see [issue #41](https://gitlab.com/lely_industries/lely-core/issues/41)).
* Create an example project for [ROS 2](https://index.ros.org/doc/ros2/), with a
  master and a (mock) slave communicating over a virtual CAN bus.
* Add support for [Zephyr](https://www.zephyrproject.org/).
* Add support for running wihthout dynamic memory allocation.
* Add TCP and UDP support to the new asynchronous I/O library and migrate
  [CAN-to-UDP](/docs/can2udp/), so the old I/O library can be removed.
* Improve the
  [test coverage](https://lely_industries.gitlab.io/lely-core/lcov/).
